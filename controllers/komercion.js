var KomercionModel = require('../models/KomercionModel');
var BaseModel = require('../models/BaseModel');
var file         = require('file-system');
var fs           = require('fs');

class Komercion{
	constructor(){

	}
	async viewKomercion(req, res){
	var komercion = await KomercionModel.getKomercion();

	komercion.forEach((item)=>{
    	var papkiAnun = 'public/postFile/' + item.id
    	
    	if(fs.existsSync(papkiAnun)){
    		item.nkar = papkiAnun + '/' + fs.readdirSync(papkiAnun)[0]
		}
	  })
	
	res.render('post',{post:komercion})
	}
	
}

module.exports = new Komercion()