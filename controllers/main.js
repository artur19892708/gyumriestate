var UserModel    = require('../models/UserModel');
var TunModel     = require('../models/TunModel');
var PostModel     = require('../models/PostModel');
var passwordHash = require('password-hash');
var file         = require('file-system');
var fs           = require('fs');



class Main{
	constructor(){

	}
	
	admin(req, res){
		res.render('viewAdmin')
	}
	main(req, res){
		res.render('viewMain')
	}
	async login(req, res){
		var data = await UserModel.find({login:req.body.login})
		data = data[0]
		if(data){
			if(passwordHash.verify(req.body.parol,data.parol)){
				req.session.user = await data
				res.redirect('/addPost')
			}
			else{
				req.session.err = 'Սխալ պառոլ'
				req.session.login = req.body.login
				res.redirect('/admin')
			}
		}
		else{
			req.session.error = 'նման գրանցվաց էջ գոյություն չունի'
			res.redirect('/admin')
		}
	}

	addPost(req, res){
		res.render('addPost')
	}

	async insertBnakaran(req, res){
		
		var verjinGrvac = await UserModel.insertBnakaran({
			// user_id     : req.session.user.id,
			vernagir    : req.body.vernagir,
			gin         : req.body.gin,
			nkaragrutyun: req.body.nkaragrutyun.trim(),
			marz        : req.body.marz,
			qaxaq       : req.body.qaxaq,
			hasce       : req.body.hasce,
			makeres     : req.body.makeres,
			gorcark_id  : req.body.vacharq,
			tesak_id    : req.body.tun,
            date        : Date.now(),
		})
		var papka = 'public/postFile/' +verjinGrvac.insertId
		if(!fs.existsSync(papka)){
			fs.mkdirSync(papka)
		}
		

		if(!(req.files.file instanceof Array)){
			var item = req.files.file
			var fileName = Date.now()+item.name
 	        item.mv(papka + '/' + fileName)
		}
		else{
			req.files.file.forEach((item)=>{
 	        var fileName = Date.now()+item.name
 	        item.mv(papka + '/' + fileName)
            })
		}

    

		res.redirect('/addPost')
	}

	async findContr(req,res){
		var opshi = {
			gorcark_id  : req.body.gorcarq,
			tesak_id    : req.body.tesak,
		}
		
		if(req.body.senyak){
			opshi.senyak=new Array(req.body.senyak)
		}
		if(req.body.qaxaq != ''){
			opshi.qaxaq = req.body.qaxaq
		}
		if(req.body.gin != ''){
			opshi.gin = req.body.gin+'<'
		}
		if(req.body.makeres != ''){
			opshi.makeres = req.body.makeres+'<'
		}
		if(req.body.poxoc != ''){
			opshi.poxoc = req.body.poxoc
		}
		var result = await TunModel.find(opshi);
		result.forEach((item)=>{
    	var papkiAnun = 'public/postFile/' + item.id
    	if(fs.existsSync(papkiAnun)){
         item.nkar = papkiAnun + '/' + fs.readdirSync(papkiAnun)[0]//readdirSync papki meji nkarnern zangvaci teskov kuda
		}
    	
    })
	res.render('post',{post:result})
	}

	async viewManramasn(req,res){
		var post=await PostModel.find({id:req.params.id})  
		res.render('manramasn',{post: post})
	}
	
}

module.exports = new Main()