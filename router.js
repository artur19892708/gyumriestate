var express   = require('express')
var Main      = require('./controllers/main')
var Tun       = require('./controllers/tun')
var Bnakaran  = require('./controllers/bnakaran')
var Komercion = require('./controllers/komercion')
var Hoxataracq= require('./controllers/hoxataracq')


var router = express.Router()

router.route('/admin').get(Main.admin)
router.route('/main').get(Main.main)
router.route('/addPost').get(Main.addPost)
router.route('/tun').get(Tun.viewTun)
router.route('/bnakaran').get(Bnakaran.viewBnakaran)
router.route('/komercion').get(Komercion.viewKomercion)
router.route('/hoxataracq').get(Hoxataracq.viewHoxataracq)
router.route('/file').get(Bnakaran.insertFile)
router.route('/manramasn/:id').get(Main.viewManramasn)



router.route('/insertBnakaran').post(Main.insertBnakaran)
router.route('/login').post(Main.login)
router.route('/tunVoronel').post(Main.findContr)










module.exports = router