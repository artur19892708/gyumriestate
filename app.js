const express    = require('express')
var bodyParser   = require('body-parser')
var passwordHash = require('password-hash');
var mysql        = require('mysql');
var ejs          = require('ejs');
var session      = require('express-session')
var router       = require('router')
const fileUpload = require('express-fileupload');
const multer     = require('multer');


const app = express()

app.set('view engine', "ejs")
app.use("/public",express.static("public"))

app.use(fileUpload());

 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
}))


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
 
var upload = multer({ storage: storage })


var router    = require('./router');
app.use('/',router)




 
app.listen(2222)