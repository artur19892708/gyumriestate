var BaseModel = require('./BaseModel')

class BnakaranModel extends BaseModel{
	constructor(){
		super()
		this.table = 'post'
	}

	getBnakaran(){
		return new Promise((res, rej)=>{
			var hraman = `select post.* from ${this.table}
			              join tesak on tesak.id = tesak_id
			              where tesak = 'bnakaran'
			              order by date desc`

			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})              
		})
	}


}

module.exports = new BnakaranModel()