var mysql  = require('mysql');

class BaseModel{
	constructor(){
		this.db = mysql.createConnection({
			host     : 'localhost',
            user     : 'root',
            password : '',
            database : 'vardan'
        });
        this.db.connect();
	}

	find(obj, table=this.table){
		return new Promise((res, rej)=>{
			var hraman = `select * from ${table} where `;
			for(let key in obj){
				if(obj[key] instanceof Array ){
					hraman+="("
					var v = obj[key].toString()

					if(v.includes('4+')){

						hraman+= `${key}>=4`
						
						v=v.substring(v.indexOf(',4+'),-2)
					
					}
					if(v!=''){
						hraman += ` or ${key} in (${v})  `
					}
					hraman+= `) and `
				}
				else{
					if(obj[key].includes('<')){
						var mec = obj[key].toString()
						mec = mec.substring(0,mec.length-1)
						hraman += `${key} <= '${mec}' and `
					}
					else{
						hraman+= ` ${key} = '${obj[key]}' and `
					}
				}	
			}
			hraman = hraman.substring(0, hraman.length-5)

			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})
		})
	}

	insertBnakaran(obj, table=this.table){
		return new Promise((res,rej)=>{
			let hraman = `insert into post(
			${Object.keys(obj).join(', ')})
			values('${Object.values(obj).join("', '")}')`

			this.db.query(hraman,function(err, data){
				if(err) throw err;
				
				res(data)
			})
		})
	}

}

module.exports = BaseModel