var BaseModel = require('./BaseModel')

class HoxataracqModel extends BaseModel{
	constructor(){
		super()
		this.table = 'post'
	}

	getHoxataracq(){
		return new Promise((res, rej)=>{
			var hraman = `select post.* from ${this.table}
			              join tesak on tesak.id = tesak_id
			              where tesak = 'hoxataracq'
			              order by date desc`

			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})              
		})
	}
}

module.exports = new HoxataracqModel()